#ifndef UTILS_H
#define UTILS_H

#include <QString>
#include <QList>
#include <QDebug>
#include <QtMath>
#include <QVectorIterator>

class Utils
{
public:
    Utils() {}
    static QString coefficientsToPolynomialString(QList<double> coefficients);
    static double calculatePolynomial(QList<double> coefficients, double x);
    static QVector<double> calculatePolynomial(QList<double> coefficients,  QVector<double> x);
    static  QVector<double>getXVector(double rangeFrom, double rangeTo);
    static const int pointsAmount = 100;
};
#endif // UTILS_H


