#-------------------------------------------------
#
# Project created by QtCreator 2015-11-30T11:30:03
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = QCustomPlotExample
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qcustomplot.cpp \
    utils.cpp

HEADERS  += mainwindow.h \
    qcustomplot.h \
    utils.h

FORMS    += mainwindow.ui
