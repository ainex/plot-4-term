#include "utils.h"


QString Utils::coefficientsToPolynomialString(QList<double> coefficients)
{
    int termPower = coefficients.size() - 1;
    QString polynomialString;
    QListIterator<double> i(coefficients);
    while (i.hasNext()){
        double coefficient = i.next();
        polynomialString.append(QString("%1").arg(coefficient < 0 ? "" : "+") + QString::number(coefficient) + "*x^" + QString::number(termPower) + " ");
        termPower--;
    }
    qDebug() << polynomialString;
    return polynomialString;

}

double Utils::calculatePolynomial(QList<double> coefficients, double x)
{
    double result = 0;
    int termPower = coefficients.size() - 1;

    QListIterator<double> i(coefficients);
    while (i.hasNext()){
        double coefficient = i.next();
        result += coefficient * qPow(x, termPower) ;
        termPower--;
    }
    qDebug() << x << " " << result;
    return result;
}

QVector<double> Utils::calculatePolynomial(QList<double> coefficients, QVector<double> xValues)
{
    QVector<double> result(xValues.size());

    for(int i =0; i < xValues.size(); i++){
        double x = xValues[i];
        result[i] = Utils::calculatePolynomial(coefficients, x);
         qDebug() << "x: " << x << "y: " << result[i];
    }

    return result;

}

QVector<double> Utils::getXVector(double rangeFrom, double rangeTo)
{
    QVector<double> result(Utils::pointsAmount);
    double step = (rangeTo - rangeFrom)/Utils::pointsAmount;
    result[0] = rangeFrom;

    for(int i = 1; i < Utils::pointsAmount; i++){
       result[i] = result[i-1] + step;
       qDebug() << "x: " << result[i];
    }
    return result;
}
