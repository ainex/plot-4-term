#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QApplication>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
  //  this->setGeometry(300,100,640,480);

    // Инициализируем объект полотна для графика ...
    wGraphic = new QCustomPlot();
    ui->gridLayout->addWidget(wGraphic,7,0,1,6); // ... и устанавливаем

    // Инициализируем вертикальную линию
    verticalLine = new QCPCurve(wGraphic->xAxis, wGraphic->yAxis);

     plotButton = ui->plotButton;


    // Подключаем сигналы событий мыши от полотна графика к слотам для их обработки
    connect(wGraphic, &QCustomPlot::mousePress, this, &MainWindow::slotMousePress);
    connect(wGraphic, &QCustomPlot::mouseMove, this, &MainWindow::slotMouseMove);
    connect(plotButton, SIGNAL (released()), this, SLOT(slotPlotButtonPress()));


    // создаём вектора для вертикальной линии
    QVector<double> x(2) , y(2);
        x[0] = 0;
        y[0] = -50;
        x[1] = 0;
        y[1] = 50;

    wGraphic->addPlottable(verticalLine);   // Добавляем линию на полотно
    verticalLine->setName("Vertical");      // Устанавливаем ей наименование
    verticalLine->setData(x, y);            // И устанавливаем координаты

    // создаём вектора для графика
    QVector<double> x1(5) , y1(5);
        x1[0] = -45;
        y1[0] = -43;
        x1[1] = 46;
        y1[1] = 42;
        x1[2] = -25;
        y1[2] = -24;
        x1[3] = -12;
        y1[3] = 10;
        x1[4] = 25;
        y1[4] = 26;

    // Добавляем график на полотно
    wGraphic->addGraph(wGraphic->xAxis, wGraphic->yAxis);
   // wGraphic->graph(0)->setData(x1,y1);     // Устанавливаем координаты точек графика

    // Инициализируем трассировщик
    tracer = new QCPItemTracer(wGraphic);
    tracer->setGraph(wGraphic->graph(0));   // Трассировщик будет работать с графиком

    // Подписываем оси координат
    wGraphic->xAxis->setLabel("x");
    wGraphic->yAxis->setLabel("y");

    // Устанавливаем максимальные и минимальные значения координат
    wGraphic->xAxis->setRange(-50,50);
    wGraphic->yAxis->setRange(-50,50);

    // Отрисовываем содержимое полотна
    wGraphic->replot();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::replot( QVector<double> xValues, QVector<double> yValues )
{


    wGraphic->graph(0)->setData(xValues,yValues);     // Устанавливаем координаты точек графика

    // Устанавливаем максимальные и минимальные значения координат
    wGraphic->xAxis->setRange(xValues[0],xValues[Utils::pointsAmount-1]);
    wGraphic->yAxis->setRange(*std::min_element(yValues.constBegin(), yValues.constEnd()), *std::max_element(yValues.constBegin(), yValues.constEnd()));

    // Отрисовываем содержимое полотна
    wGraphic->replot();
}

void MainWindow::slotMousePress(QMouseEvent *event)
{
    // Определяем координату X на графике, где был произведён клик мышью
    double coordX = wGraphic->xAxis->pixelToCoord(event->pos().x());

    // Подготавливаем координаты по оси X для переноса вертикальной линии
    QVector<double> x(2), y(2);
    x[0] = coordX;
    y[0] = -50;
    x[1] = coordX;
    y[1] = 50;

    // Устанавливаем новые координаты
    verticalLine->setData(x, y);

    // По координате X клика мыши определим ближайшие координаты для трассировщика
    tracer->setGraphKey(coordX);

    // Выводим координаты точки графика, где установился трассировщик, в lineEdit
    ui->lineEdit->setText("x: " + QString::number(tracer->position->key()) +
                          " y: " + QString::number(tracer->position->value()));

    wGraphic->replot(); // Перерисовываем содержимое полотна графика
}

void MainWindow::slotMouseMove(QMouseEvent *event)
{
    /* Если при передвижении мыши, ей кнопки нажаты,
     * то вызываем отработку координат мыши
     * через слот клика
     * */
    if(QApplication::mouseButtons()) slotMousePress(event);
}

void MainWindow::slotPlotButtonPress()
{

 QString coefficientString = ui->lineInput->text().simplified();
 QStringList coefficientsList = coefficientString.split(" ");
 double rangeFrom = ui->doubleSpinBoxStart->value();
 double rangeTo = ui->doubleSpinBoxEnd->value();
  qDebug() << "from:" << rangeFrom << "to: " << rangeTo;
 double step = (rangeTo - rangeFrom)/Utils::pointsAmount;

 qDebug() << "step:" << step;
 QList<double> coefficients;
 QListIterator<QString> i(coefficientsList);
 while (i.hasNext()){
      double coefficient = i.next().toDouble();
      coefficients.append(coefficient);

 }

 QString polynomialString = Utils::coefficientsToPolynomialString(coefficients);
 Utils::calculatePolynomial(coefficients, 2);
 ui->labelPolynomParsed->setText(polynomialString);

 QVector<double> xValues = Utils::getXVector(rangeFrom, rangeTo);
 QVector<double> yValues = Utils::calculatePolynomial(coefficients, xValues);

 replot(xValues, yValues);


}
